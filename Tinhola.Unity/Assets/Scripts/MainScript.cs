﻿using System;
using Tinhola.Agnostic;
using UnityEngine;

namespace Tinhola
{
    sealed class MainScript : MonoBehaviour
    {
        public int Difficulty { get { return _problem.Difficulty; } }
        public double Fps { get; private set; }
        public string Result { get; private set; }

        public void IncrementDifficulty(int delta)
        {
            _problem.Difficulty += delta;
        }

        public event EventHandler FpsUpdated;

        void Update()
        {
            Result = _problem.Solve().ToString();

            UpdateFps();
        }

        private void UpdateFps()
        {
            double? fpsUpdated = _fpsCounter.Update(Time.deltaTime);
            if (!fpsUpdated.HasValue)
                return;

            Fps = fpsUpdated.Value;
            var handler = FpsUpdated;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private readonly FpsCounter _fpsCounter = new FpsCounter();
        private readonly ProblemBase _problem = new NbodyProblem();
    }
}
