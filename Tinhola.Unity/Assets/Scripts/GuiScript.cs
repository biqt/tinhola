﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.Assertions;
using UI = UnityEngine.UI;

namespace Tinhola
{
    sealed class GuiScript : MonoBehaviour
    {
#pragma warning disable 649
        public UI.Text difficultyText;
        public UI.Text fpsText;
        public UI.Text resultText;
#pragma warning restore 649

        public void OnMinusButtonClick()
        {
            Assert.IsNotNull(_mainScript);

            _mainScript.IncrementDifficulty(-1);
        }

        public void OnPlusButtonClick()
        {
            Assert.IsNotNull(_mainScript);

            _mainScript.IncrementDifficulty(1);
        }

        void Start()
        {
            Assert.IsNotNull(difficultyText);
            Assert.IsNotNull(fpsText);
            Assert.IsNotNull(resultText);

            _mainScript = GetComponent<MainScript>();
            if (_mainScript != null)
            {
                _mainScript.FpsUpdated += OnFpsUpdated;
            }
        }

        void Update()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

        private void OnFpsUpdated(object sender, EventArgs e)
        {
            Assert.IsNotNull(_mainScript);

            difficultyText.text = string.Format("Difficulty: {0}", _mainScript.Difficulty);
            fpsText.text = string.Format(CultureInfo.InvariantCulture, "FPS: {0:0.00}", _mainScript.Fps);
            resultText.text = _mainScript.Result;
        }

        private MainScript _mainScript;
    }
}
