﻿namespace Tinhola.Agnostic
{
    public sealed class FpsCounter
    {
        public double? Update(double deltaSeconds)
        {
            double? result = null;
            _accumulatedFrameCount += 1;
            _accumulatedDeltaSeconds += deltaSeconds;
            if (_accumulatedDeltaSeconds > _fpsUpdatePeriodSeconds)
            {
                result = _accumulatedFrameCount / _accumulatedDeltaSeconds;
                _accumulatedFrameCount = 0;
                _accumulatedDeltaSeconds = 0.0;
            }
            return result;
        }

        private int _accumulatedFrameCount;
        private double _accumulatedDeltaSeconds;
        private const double _fpsUpdatePeriodSeconds = 1.0;
    }
}
