﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Tinhola.Agnostic
{
    public struct Solution
    {
        public Solution(int input, long timeMilliseconds, string result)
        {
            _input = input;
            _timeMilliseconds = timeMilliseconds;
            _result = result ?? string.Empty;
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{{ input: {0}, time: '{1}ms', result: '{2}' }}",
                _input, _timeMilliseconds, _result);
        }

        private readonly int _input;
        private readonly long _timeMilliseconds;
        private readonly string _result;
    }

    public abstract class ProblemBase
    {
        public ProblemBase(int difficulty)
        {
            Difficulty = difficulty;
        }

        public int Difficulty
        {
            get { return _difficulty; }
            set { _difficulty = Math.Max(0, value); }
        }

        public Solution Solve()
        {
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();
            try
            {
                var inputResult = SolveCore();
                return new Solution(inputResult.Key, stopwatch.ElapsedMilliseconds, inputResult.Value);
            }
            finally
            {
                stopwatch.Reset();
            }
        }

        protected abstract KeyValuePair<int, string> SolveCore();

        private int _difficulty;
    }

    public sealed class NbodyProblem : ProblemBase
    {
        public NbodyProblem() : base(20)
        {
        }

        protected override KeyValuePair<int, string> SolveCore()
        {
            double sqrt5 = Math.Sqrt(5);
            int input = Convert.ToInt32(Math.Pow(0.5 * (1.0 + sqrt5), Difficulty) / sqrt5);

            var bodies = new NBodySystem();
            for (int i = 0; i < input; ++i)
            {
                bodies.Advance(0.01);
            }

            var result = string.Format(CultureInfo.InvariantCulture, "{0:f6}", bodies.Energy());
            return new KeyValuePair<int, string>(input, result);
        }
    }
}
