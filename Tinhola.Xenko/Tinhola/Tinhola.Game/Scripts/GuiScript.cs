﻿using System;
using System.Globalization;
using SiliconStudio.Xenko.Engine;
using SiliconStudio.Xenko.Graphics;
using SiliconStudio.Xenko.UI;
using SiliconStudio.Xenko.UI.Controls;
using SiliconStudio.Xenko.UI.Panels;

using Debug = System.Diagnostics.Debug;

namespace Tinhola
{
    public sealed class GuiScript : StartupScript
    {
        public override void Start()
        {
            base.Start();

            SpriteFont font = Content.Load<SpriteFont>("Fonts/MainFont");
            _difficultyText = new TextBlock { Font = font, Text = "Difficulty: ?" };
            _fpsText = new TextBlock { Font = font, Text = "FPS: ?" };
            _resultText = new TextBlock { Font = font, Text = "?" };

            _mainScript = Entity.Get<MainScript>();
            if (_mainScript != null)
            {
                _mainScript.FpsUpdated += OnFpsUpdated;
            }

            // http://doc.xenko.com/latest/manual/getting-started/howto-create-an-ui.html
            var uiComponent = Entity.Get<UIComponent>();
            if (uiComponent != null)
            {
                uiComponent.RootElement = CreateLayout(font);
            }
        }

        private UIElement CreateLayout(SpriteFont font)
        {
            Debug.Assert(font != null);
            Debug.Assert(_difficultyText != null);
            Debug.Assert(_fpsText != null);
            Debug.Assert(_resultText != null);

            var minusButton = new Button { Content = new TextBlock { Text = "-", Font = font } };
            minusButton.Click += OnMinusButtonClick;
            var plusButton = new Button { Content = new TextBlock { Text = "+", Font = font } };
            plusButton.Click += OnPlusButtonClick;
            var difficultyPanel = new StackPanel { Orientation = Orientation.Horizontal, Children = { minusButton, plusButton, _difficultyText } };

            var result = new StackPanel { HorizontalAlignment = HorizontalAlignment.Left, Children = { _fpsText, difficultyPanel, _resultText } };
            return result;
        }

        private void OnMinusButtonClick(object sender, SiliconStudio.Xenko.UI.Events.RoutedEventArgs e)
        {
            Debug.Assert(_mainScript != null);

            _mainScript.IncrementDifficulty(-1);
        }

        private void OnPlusButtonClick(object sender, SiliconStudio.Xenko.UI.Events.RoutedEventArgs e)
        {
            Debug.Assert(_mainScript != null);

            _mainScript.IncrementDifficulty(1);
        }

        private void OnFpsUpdated(object sender, EventArgs e)
        {
            Debug.Assert(_mainScript != null);

            _difficultyText.Text = $"Difficulty: {_mainScript.Difficulty}";
            _fpsText.Text = string.Format(CultureInfo.InvariantCulture, "FPS: {0:0.00}", _mainScript.Fps);
            _resultText.Text = _mainScript.Result;
        }

        private MainScript _mainScript;
        private TextBlock _difficultyText;
        private TextBlock _fpsText;
        private TextBlock _resultText;
    }
}
