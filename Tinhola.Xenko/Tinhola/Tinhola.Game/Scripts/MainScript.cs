﻿using System;
using SiliconStudio.Xenko.Engine;
using SiliconStudio.Xenko.Games;
using Tinhola.Agnostic;

namespace Tinhola
{
    public sealed class MainScript : SyncScript
    {
        public int Difficulty { get { return _problem.Difficulty; } }
        public double Fps { get; private set; }
        public string Result { get; private set; } = string.Empty;

        public void IncrementDifficulty(int delta)
        {
            _problem.Difficulty += delta;
        }

        public event EventHandler FpsUpdated;

        public override void Start()
        {
            base.Start();

            _updateTime = Game.UpdateTime;
        }

        public override void Update()
        {
            Result = _problem.Solve().ToString();

            UpdateFps();
        }

        private void UpdateFps()
        {
            double? fpsUpdated = _fpsCounter.Update(_updateTime.Elapsed.TotalSeconds);
            if (!fpsUpdated.HasValue)
                return;

            Fps = fpsUpdated.Value;
            FpsUpdated?.Invoke(this, EventArgs.Empty);
        }

        private GameTime _updateTime;

        private readonly FpsCounter _fpsCounter = new FpsCounter();
        private readonly ProblemBase _problem = new NbodyProblem();
    }
}
