using SiliconStudio.Xenko.Engine;

namespace Tinhola
{
    class TinholaApp
    {
        static void Main(string[] args)
        {
            using (var game = new Game())
            {
                game.Run();
            }
        }
    }
}
